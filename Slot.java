package br.com.itau;

import java.util.Random;

public class Slot {
    // atributo
    private Opcoes opcao;

    public Slot(Opcoes opcao) {
        this.opcao = opcao;
    }

    // construtor
    public Slot() {
        Random random = new Random();
        //variavel para o tamanho da lista
        int tamanhoDeOpcoes = Opcoes.values().length;
        int valorAletorio = random.nextInt(tamanhoDeOpcoes);

        this.opcao = Opcoes.values()[valorAletorio];
    }

    //get usado para resgatar os valores
    public Opcoes getOpcao() {
        return opcao;

    }

    @Override
    public String toString() {
        String modelo = "Slot:" +
                " " + opcao +
                " pontos: "+ opcao.pontos;
        return modelo;
    }


}
