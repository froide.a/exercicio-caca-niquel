package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Maquina {
    //usado lista para montar a lista de opcoes
    //a maquina só tem 3 slots
    private List<Slot> slots;

    //a variavel int é para colocar a quantindade de slots quando instaciar na main
    public Maquina(int dificuldade) {
        //instanciamento da lista
        this.slots = new ArrayList<>();

        for (int i = 0; i <= dificuldade; i ++){
            this.slots.add(new Slot());
        }
    }

    public List<Slot> getSlots() {
        return slots
    public int calcularPontuacao(){
        int pontuacao = 0;

        //variavel slot criada apenas para o for
        for (Slot slot : this.slots){
            pontuacao = pontuacao + slot.getOpcao().pontos;

        }
        if (exiteBonus()){
            pontuacao = pontuacao*100;
        }

        return pontuacao;
    }

    //distinct é um metodo que compara os objetos para verificar se são iguais
    //se apenas 1 é distinto é que os outros 3 sao iguais
    public boolean exiteBonus(){
        boolean resposta = this.slots.stream().distinct().limit(this.slots.size()).count() == 1;

        return resposta;
    }

    // ver erro - campo opcao
    @Override
    public String toString() {
        String modelo = "Slot:" +
                " " + opcao +
                " pontos: "+ opcao.pontos;
        return modelo;
    }

}
